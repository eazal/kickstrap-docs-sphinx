KBash
=====

Kbash is a unix-style command line for Kickstrap. Because it has an environment
and ecosystem all of its own, it has been given its own
`Repository <https://github.com/ajkochanowicz/kbash>`_.

Several KBash commands are provided in Kickstrap by default, however your apps
can also define new commands. Type ``help`` to see all commands available.

Writing to Kbash from your code
-------------------------------

Unlike any console running in your browser, because Kbash is part of the
website, you can use it on any browser, whether or not it has a console
installed (E.g. an iPhone).

To do this, simply call the ``console.kbash()`` function. This works just like
``console.log()``.

To color text, you can also use the formatting function ``kbash.e()`` for
errors (red) and ``kbash.s`` for success (green)

.. code-block:: javascript

   console.kbash(kbash.s('COMPLETE') + ' The service has completed
   successfully.')

If no parameters are given, kbash.[e/s]() will write out a default statement.
So...

.. code-block:: javascript

   console.kbash(kbash.s() + ' The service has completed
   successfully.')

returns "SUCCESS: The services has completed successfully."

help
----

Lists helpful information and the commands that are running.

exit
----

Hides the kbash bar (until refresh).

less
----

Quick functions for LESS if and only if running client-side.

less watch
^^^^^^^^^^

Enable watch mode. LESS will check your LESS files for any changes and reload
the changes periodically.
