Choose a theme
==============

``theme.less`` is either where you choose your theme, write your own, or both.
To select a theme, simply write in its name as the ``@theme`` variable.

.. code-block:: html
	 
	 @theme: "spacelab";

Your theme name will correspond exactly to the folder in ``/themes/``. In this case, Kickstrap will load these two files:
``/kickstrap/themes/spacelab/theme.less``
``/kickstrap/themes/spacelab/variables.less``

If anything goes wrong after changing this variable, check that your syntax matches the above.
You will have issues if
	#. ``@theme`` is not followed by a colon.
	#. The theme name does not correspond to a theme in your ``/themes/`` folder.
	#. This line is not terminated by a semicolon.

Writing your own CSS/LESS
-------------------------

Kickstrap will load this LESS file after everything else, which means any styles written here get the final say.

Here is an example of a user using both the theme Amelia and her own tweaks.

.. code-block:: html
   
   @theme: "amelia";

   body { background-color: lavender; }
   h1 { font-size: 28pt; }
   .hero-unit { .box-shadow(3px 3px 3px black); }

Also notice the ``.box-shadow()`` mixin being used. Because ``theme.less`` is loaded last, it can also use the mixins Bootstrap's core files have provided. You can find these mixins in ``mixins.less`` in ``/kickstrap/_core/bootstrap/less/``
