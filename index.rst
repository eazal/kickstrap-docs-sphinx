.. Kickstrap documentation master file, created by
   sphinx-quickstart on Fri Dec 21 14:57:59 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   
=======================
Kickstrap Documentation
=======================

Overview
--------


Start
-----

.. toctree::
   :glob:

   start/*

Customize
---------

.. toctree::
   :maxdepth: 2

   Customize <customize>

Troubleshooting
---------------

.. toctree::
   :glob:

   troubleshooting/*

Build
-----

.. toctree::
   :maxdepth: 2

   Build <build>

Advanced
--------

.. toctree::
   :maxdepth: 2

   Advanced <advanced>

Kickstrap Developers
--------------------

.. toctree::
   :glob:

   devs/*
