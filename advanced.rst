Advanced
========

Creating an app
---------------

An app is composed of a JavaScript file called ``config.js`` and your app's JavaScript and/or CSS/LESS files.

config.js
^^^^^^^^^

Let's start by looking at the config.js file from the pinesnotify app:

.. code-block:: javascript

	define([
		'apps/pinesnotify/pinesnotify'
		, 'less!apps/pinesnotify/jquery.pnotify.default'
	], function() {})

The two essential parts of this code are the very first and very last line.

.. code-block:: javascript

	define([

and

.. code-block:: javascript

	], function() {})

This list will comprise the resources your app needs to run. These can be JavaScript, CSS, or LESS files.
In either case, **the file extension is not needed.**. This is because Require.js, the AMD loader that powers Kickstrap apps, loads files as "modules".

To denote a LESS or CSS file, simply add ``less!`` or ``css!`` to the beginning of the path. For example, Pines Notifty loads ``jquery.pnotify.default.less``, so it ``define``s:

.. code-block:: javascript

	define([
		'less!apps/pinesnotify/jquery.pnotify.default'
	], function() {})

Advanced app creation
^^^^^^^^^^^^^^^^^^^^^

The above is certainly only the beginning to how you can structure your app. Because Kickstrap uses Require.js's code from here on out, please continue reading about ``define`` and other features in the Require.js documentation.

Compiling from LESS
-------------------

Browser-specific CSS
--------------------

Using a CDN
-----------

Show upgrade message for old browsers
-------------------------------------

Sticky footer
-------------
