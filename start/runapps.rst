Run apps
========

.. sidebar:: New to JavaScript Arrays?
  
   `Learn about JavaScript arrays here
   <http://www.codecademy.com/courses/javascript-beginner-en-NhsaT/2?curriculum_id=502d635ceda910000200293b#!/exercises/0>`_

Apps can be selected by simply adding their name to the javascript array in
**kickstrap/apps.json**

Here is an example **apps.json** file running jquery, pinesnotify, and bootstrap

.. code-block:: javascript

   { apps: [
     "lib/jquery",
     "pinesnotify",
     "bootstrap"
   ]}

jQuery is loaded from the ``lib`` directory in Kickstrap's core. This keeps
common files separate
