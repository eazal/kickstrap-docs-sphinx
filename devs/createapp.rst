Create a Kickstrap App
======================

A Kickstrap app is just a directory of JavaScript and sometimes CSS files. To function as an app, a separate file is included to tell Kickstrap how to load everything when the app is called.

A basic Kickstrap app will contain at least one **JavaScript File**, and
an **app.js** file.

What happens when an app runs
-----------------------------

At run time, Kickstrap looks in **kickstrap/apps.json** for the full list of apps
to run.

.. sidebar:: Example

   If we have an app called "helloworld" with
   a script that says "Hello World,"  Kickstrap reads the app.js file at
   ``kickstrap/apps/helloworld/app.js``

   This app.js file contains require.js code to deploy all the necessary
   dependencies.

Apps can also be run on command with the ``ks.run()`` function.

Either way, Kickstrap simply uses the name of the app to find the apps'
directory containing the ``app.js`` script to run it via `require.js <http://requirejs.com>`_.
